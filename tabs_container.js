define([
	'require',
	'madjoh_modules/page_manager/page_container',
	'madjoh_modules/custom_events/custom_events',
	'madjoh_modules/cache/data_cache',
	'madjoh_modules/styling/styling',
	'madjoh_modules/page_manager/tools'
], 
function(require, PageContainer, CustomEvents, DataCache, Styling, Tools){
	var tools = new Tools();

	function TabsContainer(name, navMap, iconKeys, badgesList, defaultViewKey, issues, controllerKey, containerKey){
		TabsContainer.prototype.parent.constructor.call(this, name, defaultViewKey, issues, controllerKey, containerKey);
		this.navMap 	= navMap;
		this.iconKeys 	= iconKeys;
		this.badgesList = badgesList;
	}
	TabsContainer.prototype = new PageContainer('TabsContainer');
	TabsContainer.prototype.parent = PageContainer.prototype;
	TabsContainer.prototype.constructor = TabsContainer;

	TabsContainer.prototype.build = function(controller, container){
		TabsContainer.prototype.parent.build.call(this, controller, container);

		// LISTENERS
			var page = this;
			CustomEvents.addCustomEventListener(document, 'Notification', function(doc, types){page.onNewNotifications(types);});
	};
	TabsContainer.prototype.prepare = function(options, resetting){
		TabsContainer.prototype.parent.prepare.call(this, options, resetting);

		var types = DataCache.get('notification.types');
		if(!types) return;
		this.onNewNotifications(types);
	};

	// BADGES
		TabsContainer.prototype.onNewNotifications = function(types){
			var notificationBadges = this.nav.querySelectorAll('.tab>.tab-icon-container>.notification-badge');
			if(!notificationBadges) return console.log('Notification badge not found', this);

			for(var i = 0; i < this.badgesList.length; i++){
				if(types[this.badgesList[i]]){
					Styling.addClass(notificationBadges[i], 'active');
					notificationBadges[i].querySelector('.notification-badge-number').innerHTML = types[this.badgesList[i]];
				}
				else{
					Styling.removeClass(notificationBadges[i], 'active');
					notificationBadges[i].querySelector('.notification-badge-number').innerHTML = 0;
				}
			}
		};

	// VIEW CHANGE
		TabsContainer.prototype.onViewChange = function(view, options){
			TabsContainer.prototype.parent.onViewChange.call(this, view, options);

			var index = this.navMap[view.name];
			if(typeof index === 'undefined') return;

			var tabs 		= this.nav.querySelectorAll('.tab');
			var tabIcons 	= this.nav.querySelectorAll('.tab>.tab-icon-container>.tab-icon');
			if(!tabIcons && this.iconKeys) return console.log('Tab icons not found', this);

			for(var i = 0; i < tabs.length; i++){
				if(i === index){
					Styling.addClass(tabs[i], 'active');
					if(this.iconKeys) tools.setSource(tabIcons[i], this.iconKeys[i] + '_ACTIVE');
				}
				else{
					Styling.removeClass(tabs[i], 'active');
					if(this.iconKeys) tools.setSource(tabIcons[i], this.iconKeys[i]);
				}
			}
		};

	return TabsContainer;
});