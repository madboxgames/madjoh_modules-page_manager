define([
	'require',
	'madjoh_modules/page_manager/page'
], 
function(require, Page){
	function PageContainer(name, defaultViewKey, issues, controllerKey, containerKey){
		PageContainer.prototype.parent.constructor.call(this, name, issues, controllerKey, containerKey);
		this.defaultViewKey = defaultViewKey;
	}
	PageContainer.prototype = new Page('PageContainer');
	PageContainer.prototype.parent = Page.prototype;
	PageContainer.prototype.constructor = PageContainer;

	PageContainer.prototype.prepare = function(options, resetting, cascade){
		PageContainer.prototype.parent.prepare.call(this, options, resetting);

		if(!this.activeView.prepared || cascade) this.activeView.prepare(options, resetting);
	};

	PageContainer.prototype.show = function(){
		PageContainer.prototype.parent.show.call(this);

		if(!this.activeView.isVisible()) this.activeView.show();
	};
	PageContainer.prototype.hide = function(){
		PageContainer.prototype.parent.hide.call(this);

		if(this.activeView.isVisible()) this.activeView.hide();
	};

	PageContainer.prototype.setActiveView = function(view){
		this.activeView = view;
	};
	PageContainer.prototype.addView = function(view){
		if(!this.content) return console.log('PageContainer.content is not defined');

		this.content.appendChild(view.dom);
	};
	PageContainer.prototype.removeView = function(view){
		if(!this.content) return console.log('PageContainer.content is not defined');
		this.content.removeChild(view.dom);
		if(this.activeView === view) delete this.activeView;
	};
	PageContainer.prototype.onViewChange = function(view, options, resetting){
		this.currentViewKey = view.name;
		this.activeView 	= view;
		if(!this.prepared || JSON.stringify(this.prepare_options) !== JSON.stringify(options)) this.prepare(options, resetting);
	};
	PageContainer.prototype.onViewVisible = function(view){
		if(!this.isVisible()) this.show();
	};

	PageContainer.prototype.isContainer = function(){
		return true;
	};
	PageContainer.prototype.getView = function(){
		return this.activeView.getView();
	};

	return PageContainer;
});