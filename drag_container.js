define([
	'require',
	'madjoh_modules/page_manager/page_container',
	'madjoh_modules/page_manager/tools',
	'madjoh_modules/controls/controls',
	'madjoh_modules/styling/styling'
], 
function(require, PageContainer, Tools, Controls, Styling){
	var tools = new Tools();

	function DragContainer(name, defaultViewKey, issuesMap, controllerKey, containerKey){
		var issues = issuesMap ? Object.keys(issuesMap) : null;
		DragContainer.prototype.parent.constructor.call(this, name, defaultViewKey, issues, controllerKey, containerKey);
		this.issuesMap = issuesMap;
	}
	DragContainer.prototype = new PageContainer('DragContainer');
	DragContainer.prototype.parent = PageContainer.prototype;
	DragContainer.prototype.constructor = DragContainer;

	DragContainer.prototype.build = function(controller, container){
		DragContainer.prototype.parent.build.call(this, controller, container);

		var content = tools.getElement('div', {className : 'page active drag-container'});
		tools.appendChildren(this.dom, [content]);
		this.content = content;
		this.pages = {};

		// LISTENERS
			this.setupDragging();
	};
	DragContainer.prototype.prepare = function(options){
		DragContainer.prototype.parent.prepare.call(this, options);

		for(var i = 0; i < this.issues.length; i++){
			var page = this.pages[this.issues[i]];
				page.prepare(options);

				page.dom.style.transform 		= 'translate3d(' + i * 100 + '%, 0px, 0px)';
				page.dom.style.webkitTransform 	= 'translate3d(' + i * 100 + '%, 0px, 0px)';
		}
	};
	DragContainer.prototype.show = function(){
		DragContainer.prototype.parent.show.call(this);

		for(var i = 0; i < this.issues.length; i++){
			var page = this.pages[this.issues[i]];
			Styling.addClass(page.dom, 'active');
		}
	}

	// Bullets
		DragContainer.prototype.setupBullets = function(){
			var bulletContainer = tools.getElement('div', {className : 'bullet-container'});
			var bullets = [];
			for(var i = 0; i < this.issues.length; i++) bullets.push(tools.getElement('div', {className : 'bullet'}));
			tools.appendChildren(bulletContainer, bullets);
			tools.appendChildren(this.dom, [bulletContainer]);
		};
		DragContainer.prototype.updateBullets = function(){
			var bullets = this.dom.querySelectorAll('.bullet-container > .bullet');
			for(var i=0; i<bullets.length; i++){
				if(i === -this.position) Styling.addClass(bullets[i], 'active');
				else Styling.removeClass(bullets[i], 'active');
			}
		};

	// Dragging
		DragContainer.prototype.setupDragging = function(){
			var container = this;
			this.dom.addEventListener(Controls.pressed, 	function(e){container.dragStart(e);}, 	false);
			window.addEventListener(Controls.released,  	function(e){container.dragEnd(e);}, 	false);
			this.dom.addEventListener(Controls.cursorMove, 	function(e){container.dragging(e);}, 	false);

			this.position = 0;
			this.setupBullets();
			this.updateBullets();
		};

		DragContainer.prototype.dragStart = function(e){
			var dragSource = (Controls.pressed == 'mousedown') ? e : e.changedTouches[0];

			this.nextable = true;
			this.drag = {
				x : dragSource.clientX,
				y : dragSource.clientY
			};

			this.setSpeed(this.content, 0.06);
		};
		DragContainer.prototype.dragEnd = function(e){
			if(!this.drag) return;

			this.setSpeed(this.content, 0.3);

			var dragSource = (Controls.pressed == 'mousedown') ? e : e.changedTouches[0];
			var xRatio = (this.drag.x - dragSource.clientX) / Styling.getWidth();

			var position = this.position * 100;
			if(xRatio < -0.2 && this.position < 0){
				this.setX(this.content, position + 100);
				this.position++;
			}
			else if((xRatio > 0.2 || this.nextable) && this.position > -this.issues.length + 1){
				this.setX(this.content, position - 100);
				this.position--;
			}
			else{
				this.setX(this.content, position);
			}

			this.updateBullets();

			this.drag = null;
		};
		DragContainer.prototype.dragging = function(e){
			if(!this.drag) return;

			var dragSource = (Controls.pressed == 'mousedown') ? e : e.changedTouches[0];
			var xRatio = (this.drag.x - dragSource.clientX) / Styling.getWidth();

			if(Math.abs(xRatio) > 0.01) this.nextable = false;

			var position = this.position * 100;
			this.setX(this.content, position - xRatio*100);
		};

	// CSS Tools
		DragContainer.prototype.setX = function(object, x){
			object.style.transform 			= 'translate3d(' + x + '%, 0px, 0px)';
			object.style.webkitTransform 	= 'translate3d(' + x + '%, 0px, 0px)';
		};
		DragContainer.prototype.setSpeed = function(object, sec){
			object.style.transition 		= 'transform ' + sec + 's linear';
			object.style.webkitTransition 	= 'transform ' + sec + 's linear';
		};
		DragContainer.prototype.isDragContainer = function(){
			return true;
		};


	return DragContainer;
});