define([
	'require',
	'madjoh_modules/page_manager/page'
], 
function(require, Page){
	function Overlay(name, issues, controllerKey, containerKey){
		Overlay.prototype.parent.constructor.call(this, name, issues, controllerKey, containerKey);
	}
	Overlay.prototype = new Page('Overlay');
	Overlay.prototype.parent = Page.prototype;
	Overlay.prototype.constructor = Overlay;

	Overlay.prototype.isOverlay = function(){
		return true;
	};

	return Overlay;
});