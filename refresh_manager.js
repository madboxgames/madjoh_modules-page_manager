define([
	'require',
	'madjoh_modules/controls/controls',
	'madjoh_modules/styling/styling',
	'madjoh_modules/cache/data_cache'
],
function(require, Controls, Styling, DataCache){
	function RefreshManager(page, cacheKey, resetButtons){
		this.page 			= page;
		this.scroller 		= page.scroller;
		this.cacheKey 		= cacheKey;

		if(resetButtons && !resetButtons.length) resetButtons = [resetButtons];
		if(!resetButtons) resetButtons = [];

		// LISTENERS
			var refreshManager = this;
			this.scroller.addEventListener(Controls.released, function(){refreshManager.checkRefresh();}, false);
			for(var i = 0; i < resetButtons.length; i++) resetButtons[i].addEventListener(Controls.click, function(e){refreshManager.onResetButtonClicked(e);}, false);
	}

	RefreshManager.prototype.checkRefresh = function(){
		if(this.scroller.scrollTop < -Styling.getWidth() * 0.2) this.refresh();
	};
	RefreshManager.prototype.onResetButtonClicked = function(e){
		if(Styling.hasParent(e.target, 'corner-button')) return;
		this.reset();
	};
	RefreshManager.prototype.reset = function(force){
		if(!this.page.isVisible() && !force) return;

		if(this.scroller.scrollTop > 0){
			var scroller = this.scroller;
			scroller.style.overflow = 'hidden';
			setTimeout(function() {
			  scroller.style.overflow = '';
			}, 10);
			if(this.scroller.scrollTop > Styling.getHeight() * 2) this.scroller.scrollTop = Styling.getHeight() * 2;
			this.scroller.scrollTopSpace = 100;
			this.scrollToTop();
		}else{
			this.refresh();
		}
	};

	RefreshManager.prototype.refresh = function(){
		var cacheKey = this.page.cacheKey || this.cacheKey;
		DataCache.clear(cacheKey);
		if(this.page.lazyLoader){
			this.page.lazyLoader.startIndex = null;
		}
		if(this.page.loader) this.page.loader.show();
		this.page.prepare(this.page.prepare_options, true);
	};
	RefreshManager.prototype.scrollToTop = function(){
		if(this.scroller.scrollTop > 0){
			this.scroller.scrollTop -= this.scroller.scrollTopSpace;
			if(this.scroller.scrollTop <= 0) this.scroller.scrollTop = 0;
			else{
				var refreshManager = this;
				setTimeout(function(){refreshManager.scrollToTop();}, 10);
			}
		}
	};

	return RefreshManager;
});