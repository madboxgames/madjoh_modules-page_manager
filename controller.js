define(function(){
	function Controller(name){
		this.name = name;
	}

	//==============================//
	// 		  PAGE MANAGER API 		//
	//==============================//
		Controller.prototype.setPage = function(page){
			this.page = page;
		};

		Controller.prototype.toString = function(){
			return this.name;
		}

	return Controller;
});