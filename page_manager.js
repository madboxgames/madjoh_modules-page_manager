define([
	'require',
	'madjoh_modules/custom_events/custom_events',
	'madjoh_modules/styling/styling'
], 
function(require, CustomEvents, Styling){
	var PageManager = {
		activePages 		: {},
		activeControllers 	: {},
		stack 		: [],
		activePage 	: null,

		init : function(startPageKey, options, settings){
			PageManager.Pages 		= settings.pages;
			PageManager.Controllers = settings.controllers;

			var pagesBuilt = [];
			PageManager.buildPage(startPageKey, pagesBuilt, [], [], true);

			try{
				PageManager.goto(startPageKey, options);
			}catch(e){
				console.error(e);
			}

			for(var key in PageManager.catchers){
				CustomEvents.addCustomEventListener(document, key, PageManager.catchers[key]);
			}

			document.addEventListener('deviceready', PageManager.onDeviceReady, false);
		},

		onDeviceReady : function(){
			document.addEventListener('backbutton', function(e){PageManager.goBack(e, null, true);}, false);
		},

		catchers : {
			ChangePage : function(doc, params){
				var key = params.key;
				delete params.key;

				try{
					PageManager.goto(key, params);
				}catch(e){
					console.error(e);
				}
			},
			BackPage : function(doc, params){PageManager.goBack(null, params);},
			RebuildPages : function(doc, params) {
				for(var pageKey in PageManager.activePages){
				    PageManager.activePages[pageKey].build(PageManager.activePages[pageKey].controller, PageManager.activePages[pageKey].container);
				}
			}
		},

		goto : function(pageKey, options, isBack){
			if(!options) options = {};

			var page = PageManager.activePages[pageKey];
			if(!page){
				console.log('Page not found in active pages', pageKey);
				var pagesBuilt = [];
				PageManager.buildPage(pageKey, pagesBuilt, [], [], true);
				page = PageManager.activePages[pageKey];
			}
			page = page.getView();
			if(page.isOverlay()) options.stack = true;
			if(isBack) options.isBack = true;

			if(PageManager.activePage && PageManager.activePage.name === page.name && PageManager.activePage.isSame(options)){
				PageManager.activePage.refresh(PageManager.activePage.prepare_options);
				return;
			}

			if(PageManager.activePage){
				if(PageManager.activePage.isOverlay() || !page.isOverlay()){
					page.lock();
					PageManager.activePage.hide(PageManager.activePage === page);
					page.unlock();
				}
			}

			options.stackLength = PageManager.stack.length;
				 if(isBack) 		options.stackLength--;
			else if(options.stack) 	options.stackLength++;
			else if(!options.isTab)	options.stackLength = 1;

			if(!page.isPrepared() || JSON.stringify(page.prepare_options) !== JSON.stringify(options)) page.prepare(options);

			PageManager.activePage = page;

				 if(isBack) 		PageManager.stack.pop();
			else if(options.stack) 	PageManager.stack.push({key : pageKey, options : options});
			else if(options.isTab){
				PageManager.stack.pop();
				PageManager.stack.push({key : pageKey, options : options});
			}
			else 					PageManager.stack = [{key : pageKey, options : options}];

			PageManager.clean(page);

			if(!page.isVisible()) page.show();
		},

		refresh : function(){
			if(PageManager.stack && PageManager.stack.length > 0){
				var options = PageManager.stack[PageManager.stack.length -1].options;
				PageManager.activePage.refresh(options);
			}
		},
		goBack : function(e, params, isAndroidBack){
			if(e && e.preventDefault) e.preventDefault();

			if(isAndroidBack){
				var spinnerPage = document.getElementById('spinner-page');
				if(spinnerPage && Styling.hasClass(spinnerPage, 'active')) return false;
			}

			if(PageManager.stack.length < 2) return console.log('Cannot go back !', PageManager.stack);
			var previousParams = PageManager.stack[PageManager.stack.length - 2];

			if(params){
				for(var key in params) previousParams.options[key] = params[key];
			}

			PageManager.goto(previousParams.key, previousParams.options, true);
		},

		clean : function(page){
			var pagesBuilt 			= [];
			var pagesToRemove 		= Object.keys(PageManager.activePages);
			var controllersToRemove = Object.keys(PageManager.activeControllers);

			PageManager.buildPage(page.name, pagesBuilt, pagesToRemove, controllersToRemove, true);

			// KEEP PREVIOUS PAGE
			if(PageManager.stack.length > 1){
				PageManager.buildPage(PageManager.stack[PageManager.stack.length - 2].key, pagesBuilt, pagesToRemove, controllersToRemove, false);
			}

			for(var i = 0; i < pagesToRemove.length; i++){
				delete PageManager.activePages[pagesToRemove[i]];
			}
			for(var i = 0; i < controllersToRemove.length; i++){
				delete PageManager.activeControllers[controllersToRemove[i]];
			}
		},
		buildPage : function(pageKey, pagesBuilt, pagesToRemove, controllersToRemove, isActive){
			if(!PageManager.Pages[pageKey]) return console.log('Page ' + pageKey + ' does not exist !', pageKey, PageManager.Pages);

			if(pagesBuilt.indexOf(pageKey) > -1) return PageManager.activePages[pageKey];
			pagesBuilt.push(pageKey);

			var index = pagesToRemove.indexOf(pageKey);
			if(index > -1) pagesToRemove.splice(index, 1);

			var page 			= (PageManager.activePages[pageKey]) ? PageManager.activePages[pageKey] : new PageManager.Pages[pageKey]();
			var controllerKey 	= page.controllerKey;
			var controller 		= null;
			var isNewPage 		= (typeof PageManager.activePages[pageKey] === 'undefined');

			if(isNewPage) PageManager.activePages[pageKey] = page;

			// Build Controller
			if(controllerKey){
				if(!PageManager.Controllers[controllerKey]) return console.log('Controller Not Found', controllerKey, PageManager.Controllers);

				var index = controllersToRemove.indexOf(controllerKey);
				if(index > -1) controllersToRemove.splice(index, 1);

				if(!PageManager.activeControllers[controllerKey]) PageManager.activeControllers[controllerKey] = new PageManager.Controllers[controllerKey]();

				controller = PageManager.activeControllers[controllerKey];
			}

			// Build Container
			if(page.containerKey !== 'ViewContainer') PageManager.buildPage(page.containerKey, pagesBuilt, pagesToRemove, controllersToRemove, isActive);

			// Build the page
			if(isNewPage) page.build(controller, PageManager.activePages[page.containerKey]);

			// Build Default View
			if(page.currentViewKey){
				var activeView = PageManager.buildPage(page.currentViewKey, pagesBuilt, pagesToRemove, controllersToRemove, isActive);
				page.setActiveView(activeView);
			}
			else if(page.defaultViewKey){
				var activeView = PageManager.buildPage(page.defaultViewKey, pagesBuilt, pagesToRemove, controllersToRemove, isActive);
				page.setActiveView(activeView);
			}

			// Build Issues for active view
			if(isActive || page.isDragContainer()){
				var extendActivity = page.isDragContainer();
				for(var i = 0; i < page.issues.length; i++) PageManager.buildPage(page.issues[i], pagesBuilt, pagesToRemove, controllersToRemove, extendActivity);	
			}

			return page;
		}
	};

	return PageManager;
});