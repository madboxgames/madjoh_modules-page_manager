define([
	'require',
	'madjoh_modules/page_manager/tools',
	'madjoh_modules/styling/styling',
	'madjoh_modules/custom_events/custom_events'
], 
function(require, Tools, Styling, CustomEvents){
	var tools = new Tools();

	function Page(name, issues, controllerKey, containerKey){
		this.name 			= name;
		this.issues 		= (issues) ? issues : [];
		this.controllerKey 	= controllerKey;
		this.containerKey 	= containerKey ? containerKey : 'ViewContainer';

		this.controller 	= null;
		this.container 		= null;
		this.dom 			= null;
	}
	Page.prototype.constructor = Page;

	//==============================//
	// 		  PAGE MANAGER API 		//
	//==============================//
		Page.prototype.build = function(controller, container){
			this.controller = controller;

			var isWholePage = (this.containerKey === 'ViewContainer');
			this.dom = tools.getPage(isWholePage);
			if(!isWholePage) this.container = container;

			if(this.container && this.container.isDragContainer()){
				this.container.pages[this.name] = this;
			}
		};

		Page.prototype.prepare = function(options, resetting){
			this.prepared = true;

			var isWholePage = (this.containerKey === 'ViewContainer');
			if(isWholePage) document.getElementById('p_body').appendChild(this.dom);
			else 			this.container.addView(this);

			if(this.container) this.container.onViewChange(this, options, resetting);

			if(!this.isContainer() && this.controller && this.controller.setPage){
				this.controller.setPage(this);
			}

			if(window.cordova && Styling.isIOS()) StatusBar.styleDefault();

			var isSame = false;
			try{isSame = JSON.stringify(this.prepare_options) !== JSON.stringify(options);}
			catch(e){console.log(e);}

			if(!isSame) this.scrollTop = 0;
			this.prepare_options = options;

			if(this.scroller && this.scrollTop) this.scroller.scrollTop = this.scrollTop;
		};
		Page.prototype.refresh = function(options){
			if(this.refreshManager){
				this.scrollTop = 0;
				this.scrollTo = 0;
				this.prepare(options);
			}
		};

		Page.prototype.destroy = function(){
			this.prepared = false;
			if(!this.dom.parentNode) return;

			if(this.containerKey === 'ViewContainer') 	document.getElementById('p_body').removeChild(this.dom);
			else 										this.container.removeView(this);
		};

		Page.prototype.show = function(){
			if(!this.isVisible() && !this.isContainer()) CustomEvents.fireCustomEvent(document, 'LogEvent', {event : 'ChangePage', type : this.name});
			Styling.addClass(this.dom, 'active');
			if(this.container) this.container.onViewVisible(this);
		};

		Page.prototype.lock = function(){
			if(this.locked) return;
			this.locked = true;
			if(this.container && !this.container.locked) this.container.lock();
		};
		Page.prototype.unlock = function(){
			if(!this.locked) return;
			this.locked = false;
			if(this.container && this.container.locked) this.container.unlock();
		};
		Page.prototype.hide = function(){
			if(this.locked) return;

			this.prepared = false;

			if(this.scroller) this.scrollTop = this.scroller.scrollTop;

			Styling.removeClass(this.dom, 'active');

			if(this.container && this.container.isVisible()) this.container.hide();

			CustomEvents.fireCustomEvent(this, 'HIDE');
			if(this.lazyLoader) this.lazyLoader.startIndex = null;

			this.destroy();
		};

		Page.prototype.isPrepared = function(){
			return this.prepared;
		};
		Page.prototype.isVisible = function(){
			return Styling.hasClass(this.dom, 'active');
		};
		Page.prototype.isContainer = function(){
			return false;
		};
		Page.prototype.isOverlay = function(){
			return false;
		};
		Page.prototype.isDragContainer = function(){
			return false;
		};
		Page.prototype.getView = function(){
			return this;
		};
		Page.prototype.isSame = function(options){
			return true;
		};

		Page.prototype.toString = function(){
			return this.name;
		};

	return Page;
});