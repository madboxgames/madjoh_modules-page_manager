define([
	'require',
	'madjoh_modules/wording/wording',
	'madjoh_modules/styling/styling',
	'madjoh_modules/external_links/external_links',
	'madjoh_modules/images/images'
], 
function(require, Wording, Styling, ExternalLinks, Images){
	function Tools(){}

	//==========================//
	// 	   GENERIC OBJECTS 		//
	//==========================//
		Tools.prototype.getElement = function(type, options){
			if(!options) options = {};

			var element = document.createElement(type);
			for(var key in options){
				if(typeof options[key] !== 'undefined') element[key] = options[key];
			}
			return element;
		};
		Tools.prototype.getPage = function(isWholePage){
			var pageClass = 'page';
			if(isWholePage && Styling.isIOS()) pageClass += ' whole-page';
			return this.getElement('div', {className : pageClass});
		};
		Tools.prototype.getTextElement = function(textKey, type, originalOptions){
			if(!type) type = 'div';
			var options = {};
			for(var key in originalOptions) options[key] = originalOptions[key];

			var values = options.values;
			delete options.values;

			var text = (options.rawText) ? Wording.prettyBigNumbers(textKey) : Wording.getText(textKey, values);

			if(options.upperCase){
				text = text.toUpperCase();
				delete options.upperCase;
			}

			var element = this.getElement(type, options);
			
			element.innerHTML = text;
			return element;
		};
		Tools.prototype.getSvgImage = function(imageKey, options, color){
			if(!options) options = {};
			options.className = (options.className) ? 'img-container ' + options.className : 'img-container';
			var imageContainer = this.getElement('div', options);
				var image = this.getElement('object');
					image.type = 'image/svg+xml';
					image.data = Images.sources[imageKey];

					image.setColor = function(){
						image.loaded = true;

						if(!image.styleData) return;

						var svgDoc = image.contentDocument;
						if(!svgDoc) return;
						var styleElement = svgDoc.querySelector('.svg-style');

						if(!styleElement){
							styleElement = svgDoc.createElementNS('http://www.w3.org/2000/svg', 'style');
							var svgElement = svgDoc.querySelector('svg');
							if(!svgElement) return;
							svgElement.appendChild(styleElement);
						}

						var text = '* {';
						for(var type in image.styleData){
							text += type + ' : ' + image.styleData[type] + ';';
						}
						text += '}';
						styleElement.textContent =  text;
						styleElement.className = 'svg-style';

						return;
					};

					image.addEventListener('load', function(){image.setColor();}, false);

				var imageCalque = this.getElement('div', {className : 'img-calque'});
				this.appendChildren(imageContainer, [image, imageCalque]);

			if(color) this.setImageColor(imageContainer, color);

			return imageContainer;
		};
		Tools.prototype.getImage = function(imageKey, options){
			if(!options) options = {};
			options.className = (options.className) ? 'img-container ' + options.className : 'img-container';

			if(!Images.sources[imageKey]) console.log('Image not found', imageKey);

			var image = this.getElement('img', options);
				image.src = 'app/images/' + Styling.getSizeCategory() + '/' + Images.sources[imageKey];
			return image;
		};
		Tools.prototype.setSvgSource = function(image, imageKey, color){
			var object = image.querySelector('object');
				object.addEventListener('load', function(e){e.target.loaded = true;}, false);
				object.loaded = false;
				object.data = Images.sources[imageKey];

			if(color) this.setImageColor(image, color);
		};
		Tools.prototype.setSource = function(image, imageKey){
			if(!Images.sources[imageKey]) console.log('Image not found', imageKey);
			image.src = 'app/images/' + Styling.getSizeCategory() + '/' + Images.sources[imageKey];
		};
		Tools.prototype.setImageColor = function(image, color, type){
			var object = image.querySelector('object');
			if(!type) type = 'fill';

			if(!object.styleData) object.styleData = {};
			object.styleData[type] = color;
			object.setColor();
		};
		Tools.prototype.getLink = function(textKey, href){
			var linkText = this.getTextElement(textKey, 'span');

			var link = this.getElement('a', {target : '_blank', href : href});
				link.appendChild(linkText);
				link.addEventListener('click', ExternalLinks.handleURL, false);

			return link;
		};
		Tools.prototype.getImageLink = function(imageKey, href, color){
			var image = this.getImage(imageKey, null, color);

			var link = this.getElement('a', {target : '_blank', href : href});
				link.appendChild(image);
				link.addEventListener('click', ExternalLinks.handleURL, false);

			return link;
		};
		Tools.prototype.getForm = function(options){
			options.method = 'post';
			
			var form = this.getElement('form', options);
				form.addEventListener('submit', function(e){e.preventDefault();}, false);
			
			return form;
		};
		Tools.prototype.getInput = function(type, name, placeholderKey, value, options){
			if(!options) options = {};
				options.type 	= type;
				options.name 	= name;
				options.value 	= value;
			if(placeholderKey){
				options.placeholder = Wording.getText(placeholderKey);
				if(options.upperCase) options.placeholder = options.placeholder.toUpperCase();
			}

			return this.getElement('input', options);
		};
		Tools.prototype.getTextArea = function(options){
			var onFocus = function(e){
				var position = Styling.getPositionInPage(e.target);
				setTimeout(function(){
					document.getElementById('p_body_container').scrollTop = position.top - Styling.getHeight()*0.05;
				}, 100);
			};

			if(options.placeholder) options.placeholder = Wording.getText(options.placeholder);

			var textArea = this.getElement('textarea', options);
				if(Styling.isAndroid()) textArea.addEventListener('focus', 	onFocus, true);
			return textArea;
		};

	//==========================//
	// 	   DOM MANIPULATION 	//
	//==========================//
		Tools.prototype.appendChildren = function(element, children){
			for(var i = 0; i < children.length; i++){
				if(children[i]) element.appendChild(children[i]);
			}
		};

	return Tools;
});